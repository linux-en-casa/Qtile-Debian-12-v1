# Qtile en Debian GNU/Linux 12 Bookworm

Estos son los archivos de configuración que he usado en el Portátil H.P. ProBook 430 G3 durante el mes de Diciembre de 2023.

## Bitácora
La instalación de Qtile la hice siguiendo el video de Linux Dabbler:
[Linux Dabbler - Qtile](https://youtu.be/IoCwRZkm9l8?si=0iVh6NkBlTkla0-1) .

Después de varios intentos en directo, en este video finalmente pude hacer la instalación:
[Linux Debian 12 con Qtile](https://youtube.com/live/Dk_UmUvKDrg) .

En los comentarios de ese video están los comandos que utilicé.

Posteriormente tomé algunos atajos y configuraciones de la iso de Arco Linux con Qtile.
Y del canal de YT [My Linux For Work](https://www.youtube.com/@mylinuxforwork)

Y otros que ya no recuerdo.

## Configuración
Casi todo se configura con el archivo ~/.config/qtile/config.py

Tengo 7 versiones del mismo archivo a medida que iba progresando en la configuración. 
Al final tengo una configuración con 3 Pantallas pero tengo algo llamado "Fake Screens" en que creo 4 "screens falsas".
Una de las pantallas externas la divido en 2 screens dejando un espacio para el screenkey que siempre uso en los videos.

Tengo un archivo autostart_x11.sh en el que llamo a Nitrogen para el fondo de pantalla y a picom para manejar las transparencias y el bordeado de las ventanas en algunos layouts.

También uso arandr y los scripts de ~/.screenlayout para la configuración de las pantallas que se llaman desde el archivo autostart.


## Notas
* Esta configuración es en X11, en Wayland la he probado usando Debian 13 - Trixie, pero no la he podido configurar bien y ni siquiera guardé los archivos :-(
* Para lanzar aplicaciones uso dmenu y rofi.  No tengo ningún archivo de configuración de rofi, solo configuro los atajos de teclado directo en el archivo de configuración de Qtile.
* El archivo de configuración de cava no tiene nada que ver con esta configuración, solo lo dejo para que no se me olvide después.

## Links adicionales:
Más videos y Directos con la configuración en YT:
[YouTube, Linux en Casa](https://www.youtube.com/LinuxenCasa) .
 
También en Twitch:
[Twitch, Linux en Casa](https://www.twitch.tv/linuxencasa) .


**GNU General Public License v3.0**

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.


